# Generated by Django 2.1.5 on 2020-02-26 10:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('homepage', '0003_auto_20200226_1558'),
    ]

    operations = [
        migrations.AlterField(
            model_name='year',
            name='year',
            field=models.CharField(default='NULL', max_length=10),
        ),
    ]
