from django import forms
from .models import Friends,Year

class Friends_Form(forms.ModelForm):
    class Meta:
        model = Friends
        fields=['name','hobby','favorite_drink','favorite_food','year']
