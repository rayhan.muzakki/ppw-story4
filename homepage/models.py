from django.db import models

class Year(models.Model):
      year=models.CharField(max_length=10,default='NULL')

      def __str__(self):
            return self.year

# Create your models here.

class Friends(models.Model):
      name= models.CharField(max_length=100)
      hobby=models.CharField(max_length=100)
      favorite_food=models.CharField(max_length=100)
      favorite_drink=models.CharField(max_length=100)
      year=models.ForeignKey(Year,on_delete=models.CASCADE)

      def __str__(self):
            return self.name

