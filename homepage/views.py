from django.shortcuts import redirect, render
from .forms import Friends_Form
from .models import Friends
from django.http import HttpResponseRedirect, request

# Create your views here.
def home(request):
    return render(request, 'home.html') 

def profile(request):
    return render(request, 'profile.html')

def contact(request):
    return render(request, 'contact.html')

def challenge(request):
    friend = Friends.objects.all()
    form= Friends_Form()
    if (request.method=='POST'):
        form = Friends_Form(request.POST)
        if(form.is_valid()):
            form.save()
        else:
            form= Friends_Form()
    return render(request, 'challenge.html',{'form': form,'friends':friend})

